// const { password } = require('pg/lib/defaults')
// const { response } = require('express')
// const { user } = require('pg/lib/defaults')
const model = require('../models'),
  { genSalt, hash, compareSync } = require('bcrypt'),
  jwt = require('jsonwebtoken');

const cryptPassword = async (password) => {
  const salt = await genSalt(12);

  return hash(password, salt);
}; // fungsi bikin enkripsi password

module.exports = {
  register: async (req, res) => {
    try {
      const data = await model.user.create({
        ...req.body, // (...) spread operator
        password: await cryptPassword(req.body.password),
      }); // fungsi bikin register

      return res.status(200).json({
        success: true,
        error: 0,
        message: 'user successfully registered',
        data: data,
      });
    } catch (error) {
      return res.status(500).json({
        success: false,
        error: error.code,
        message: error,
        data: null,
      });
    }
  },
  login: async (req, res) => {
    try {
      const userExists = await model.user.findOne({
        where: {
          username: req.body.username,
        },
      });

      if (!userExists)
        return res.status(404).json({
          success: false,
          error: error.code,
          message: 'User not found',
          data: null,
        });

      if (compareSync(req.body.password, userExists.password)) {
        const token = jwt.sign({ id: userExists.id, username: userExists.username, email: userExists.email }, 'password123', { expiresIn: '12h' });

        return res.status(200).json({
          success: true,
          error: 0,
          message: 'user successfully login',
          data: {
            token: token,
          },
        });
      }

      return res.status(409).json({
        success: false,
        error: 409,
        message: 'Invalid credentials',
        data: null,
      });
    } catch (error) {
      return res.status(500).json({
        success: false,
        error: error.code,
        message: error,
        data: null,
      });
    }
  },
};
