const express = require('express'); // inisiasi variable yang berisi express
const router = express.Router(); // inisiasi variable yang berisi fungsi router express
const { register, login } = require('../controllers/userController.js'); // inisiasi object controller
const validate = require('../middleware/validate');
const { registerRules } = require('../validators/rule');

router.post('/register', validate(registerRules), register); // route untuk endpoint register
router.post('/login', login); // route untuk endpoint login

module.exports = router; // export fungsi router agar module lain bisa membaca file ini
